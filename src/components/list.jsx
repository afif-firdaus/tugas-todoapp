import React from "react";
import { Form, FormGroup, Label, Input } from 'reactstrap';

class List extends React.Component {
  render() {

    const formList = {
      marginLeft: "30px",
      marginTop: "60px",
      display: "flex"
    }

    return (
      <Form>
        <FormGroup check inline style={formList}>
          <Label check>
            <Input type="checkbox" /> Afternoon Class
          </Label>
        </FormGroup>
      </Form>
    );
  }
}

export default List;
