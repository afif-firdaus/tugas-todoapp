import List from "./list.jsx";
import React from "react";
import { Button, ButtonToggle } from 'reactstrap'

class ToDoItem extends React.Component {
  render() {

    const cardItem = {
      width: "70%",
      margin: "30px auto",
      minHeight: "600px",
      boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)",
      borderRadius: "5px",
      textAlign: "left"
    }

    const buttonstyle1 = {
      marginTop: "25px",
      marginLeft: "30px"
    }

    const buttonstyle2 = {
      marginTop: "25px",
      marginLeft: "10px"
    }

    return (
      <div style={cardItem}>
        <ButtonToggle color="info" style={buttonstyle1}>Add Task</ButtonToggle>
        <Button outline color="info" style={buttonstyle2}>Add Section</Button>
        <List />
        <hr style={{ width: "97%" }} />
        <List />
        <hr style={{ width: "97%" }} />
        <List />
        <hr style={{ width: "97%" }} />
        <List />
        <hr style={{ width: "97%" }} />
      </div>
    );
  }
};

export default ToDoItem;
