import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

const NavBar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  const navstyle = {
    marginLeft: "30px",
    color: "white"
  }

  return (
    <div>
      <Navbar color="info" light expand="md">
        <NavbarBrand href="/" style={navstyle}>My Tasks</NavbarBrand>
      </Navbar>
    </div >
  );
}

export default NavBar;
