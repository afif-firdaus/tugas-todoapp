import './App.css';
import React, {Component} from 'react';
import ToDoItem from "./components/todoitem.jsx";
import NavBar from "./components/navbar.jsx";

function App() {
  return (
    <div className="App">
      <NavBar />
      <ToDoItem />
    </div>
  );
}

export default App;
